ConsoleRoulette
==============
coding-exercise by Gamesys
--------------
*Completed by Tomas Zaoral*

* Program expects one and only argument - path to file with user data.
* Dealing with money only BigDecimal should be used, but for the sake of exercise using doubles.
* Since this is a console application all errors are printed to stderr (in normal standard application/back-end a common logging framework would be used).
* Invalid/unparsable bets or bets from nonexistent players are ignored (error printed to stderr), one player can place more bets per round.
* For the sake of exercise using java.util.Random, in real production code, proprietary RNG would be used as discussed during interview.
* Even though 0 is from mathematical point of view an even number, bet placed on EVEN is assumed to be a loss when 0 is drawn. Otherwise the house would be loosing on EVEN bets in long run.
* Application will run until it receives an EOF, then, when all bets are processed (last roulette spin), application will exit.
* If input files contains user name duplicates, only the first one is taken into account (and warning is printed to stderr).