package uk.co.gamesys.interview.zaoral.console;

import org.junit.Test;
import uk.co.gamesys.interview.zaoral.console.ConsoleRoulette;

public class ConsoleRouletteTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void throws_iae_for_bad_number_of_arguments() {
		ConsoleRoulette.main();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throws_iae_for_bad_number_of_arguments2() {
		ConsoleRoulette.main("", "");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throws_iae_for_bad_number_of_arguments3() {
		ConsoleRoulette.main("", "", "");
	}
}
