package uk.co.gamesys.interview.zaoral.roulette;

import org.junit.Test;

import com.google.common.testing.EqualsTester;
import uk.co.gamesys.interview.zaoral.roulette.Outcome;
import uk.co.gamesys.interview.zaoral.roulette.ProcessedBet;

public class ProcessedBetTest {
	private static final ProcessedBet PROCESSED_BET_1 = ProcessedBet.newBuilder()
			.setBetAmount(10)
			.setBetTarget(2)
			.setOutcome(Outcome.LOSS)
			.setUserName("username")
			.setWinnings(200)
			.build();
	private static final ProcessedBet PROCESSED_BET_2 = ProcessedBet.newBuilder()
			.setBetAmount(10)
			.setBetTarget(3)
			.setOutcome(Outcome.LOSS)
			.setUserName("username")
			.setWinnings(200)
			.build();
	private static final ProcessedBet PROCESSED_BET_3 = ProcessedBet.newBuilder()
			.setBetAmount(10)
			.setBetTarget(2)
			.setOutcome(Outcome.WIN)
			.setUserName("username")
			.setWinnings(200)
			.build();
	private static final ProcessedBet PROCESSED_BET_4 = ProcessedBet.newBuilder()
			.setBetAmount(10)
			.setBetTarget(2)
			.setOutcome(Outcome.LOSS)
			.setUserName("name")
			.setWinnings(200)
			.build();
	private static final ProcessedBet PROCESSED_BET_5 = ProcessedBet.newBuilder()
			.setBetAmount(10)
			.setBetTarget(2)
			.setOutcome(Outcome.LOSS)
			.setUserName("username")
			.setWinnings(0)
			.build();
	
	@Test
	public void test_equals_contract() {
		new EqualsTester()
			.addEqualityGroup(PROCESSED_BET_1, PROCESSED_BET_1, PROCESSED_BET_1)
			.addEqualityGroup(PROCESSED_BET_2, PROCESSED_BET_2)
			.addEqualityGroup(PROCESSED_BET_3, PROCESSED_BET_3)
			.addEqualityGroup(PROCESSED_BET_4, PROCESSED_BET_4)
			.addEqualityGroup(PROCESSED_BET_5, PROCESSED_BET_5)
			.addEqualityGroup("word", "word")
			.testEquals();
	}
}
