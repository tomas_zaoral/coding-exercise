package uk.co.gamesys.interview.zaoral.roulette;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static uk.co.gamesys.interview.zaoral.roulette.Outcome.WIN;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import uk.co.gamesys.interview.zaoral.roulette.GameResults;
import uk.co.gamesys.interview.zaoral.roulette.Player;
import uk.co.gamesys.interview.zaoral.roulette.Players;
import uk.co.gamesys.interview.zaoral.roulette.ProcessedBet;

public class PlayersTest {
	private static final List<Player> EXPECTED_PLAYERS = asList(
			new Player("ricky", 10, 10),
			new Player("vilma", 1, 0),
			new Player("tetris_god", 0, 0));
	private static final GameResults GAME_RESULTS = new GameResults(10, asList(
			ProcessedBet.newBuilder()
				.setBetAmount(10)
				.setBetTarget(2)
				.setOutcome(WIN)
				.setUserName("ricky")
				.setWinnings(20)
				.build(),
			ProcessedBet.newBuilder()
				.setBetAmount(20)
				.setBetTarget(2)
				.setOutcome(WIN)
				.setUserName("vilma")
				.setWinnings(10)
				.build()));
	private static final List<Player> UPDATED_PLAYERS = asList(
			new Player("ricky", 30, 20),
			new Player("vilma", 11, 20),
			new Player("tetris_god", 0, 0));
	private final String usersBad1 = getClass().getResource("users_bad1.txt").getPath();
	private final String usersBad2 = getClass().getResource("users_bad2.txt").getPath();
	private final String usersBad3 = getClass().getResource("users_bad3.txt").getPath();
	private final String usersBad4 = getClass().getResource("users_bad4.txt").getPath();
	private final String usersBad5 = getClass().getResource("users_bad5.txt").getPath();
	private final String usersCorrect = getClass().getResource("users_correct.txt").getPath();

	@Test
	public void bad_user_data1() {
		assertFalse(new Players(usersBad1).iterator().hasNext());
	}

	@Test
	public void bad_user_data2() {
		assertFalse(new Players(usersBad2).iterator().hasNext());
	}

	@Test
	public void bad_user_data3() {
		assertFalse(new Players(usersBad3).iterator().hasNext());
	}

	@Test
	public void bad_user_data4() {
		assertFalse(new Players(usersBad4).iterator().hasNext());
	}

	@Test
	public void bad_user_data5() {
		assertFalse(new Players(usersBad5).iterator().hasNext());
	}
	
	@Test
	public void correct_user_data() {
		final Iterator<Player> loadedPlayers = new Players(usersCorrect).iterator();
		final Iterator<Player> expectedPlayers = EXPECTED_PLAYERS.iterator();
		
		while (loadedPlayers.hasNext() && expectedPlayers.hasNext()) {
			assertEquals(expectedPlayers.next(), loadedPlayers.next());
		}
		
		assertFalse(loadedPlayers.hasNext() || expectedPlayers.hasNext());
	}
	
	@Test
	public void test_contain() {
		final Players players = new Players(usersCorrect);
		assertTrue(players.contain("tetris_god")
				&& players.contain("vilma")
				&& players.contain("ricky"));
		assertFalse(players.contain("not-there")
				|| players.contain("nonexistent"));
	}
	
	@Test
	public void test_update_with_results() {
		final Players loadedPlayers = new Players(usersCorrect);
		loadedPlayers.updateWithResults(GAME_RESULTS);
		final Iterator<Player> players = loadedPlayers.iterator();
		final Iterator<Player> expectedPlayers = UPDATED_PLAYERS.iterator();
		
		while (players.hasNext() && expectedPlayers.hasNext()) {
			assertEquals(expectedPlayers.next(), players.next());
		}
		
		assertFalse(players.hasNext() || expectedPlayers.hasNext());
	}
	
	@Test
	public void find_longest_name() {
		final Players players1 = new Players(usersCorrect);
		assertEquals("tetris_god".length(), players1.getLongestNameLength());
	}
}
