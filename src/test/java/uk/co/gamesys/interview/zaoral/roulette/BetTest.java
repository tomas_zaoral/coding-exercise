package uk.co.gamesys.interview.zaoral.roulette;

import org.junit.Test;

import com.google.common.testing.EqualsTester;
import uk.co.gamesys.interview.zaoral.roulette.Bet;

public class BetTest {
	
	@Test
	public void test_equals_contract() {
		new EqualsTester()
			.addEqualityGroup(new Bet("paul", 15, 10.0), new Bet("paul", 15, 10.0), new Bet("paul", 15, 10.0))
			.addEqualityGroup(new Bet("ricky", 15, 10), new Bet("ricky", 15, 10))
			.addEqualityGroup(new Bet("ricky", 1, 10), new Bet("ricky", 1, 10))
			.addEqualityGroup(new Bet("ricky", 15, 1), new Bet("ricky", 15, 1))
			.addEqualityGroup("word", "word")
			.testEquals();
	}
}
