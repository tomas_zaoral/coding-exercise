package uk.co.gamesys.interview.zaoral.roulette;

import static java.util.Optional.empty;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.function.Function;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import uk.co.gamesys.interview.zaoral.roulette.Bet;
import uk.co.gamesys.interview.zaoral.roulette.Roulette;
import uk.co.gamesys.interview.zaoral.roulette.StreamBetFeeder;

@RunWith(MockitoJUnitRunner.class)
public class StreamBetFeederTest {
	private static final Optional<Bet> BET = Optional.of(new Bet("user", 37, 100));
	@Mock
    Roulette roulette;
	@Mock
	Function<String, Optional<Bet>> betParser;
	final InputStream stream = new ByteArrayInputStream("string1\nstring2".getBytes(StandardCharsets.UTF_8));
	
	@Test(expected = NullPointerException.class)
	public void throws_npe_for_null_input_stream() {
		new StreamBetFeeder(null, roulette, betParser);
	}
	
	@Test(expected = NullPointerException.class)
	public void throws_npe_for_null_roulette_engine() {
		new StreamBetFeeder(stream, null, betParser);
	}
	
	@Test(expected = NullPointerException.class)
	public void throws_npe_for_null_bet_parser() {
		new StreamBetFeeder(stream, roulette, null);
	}
	
	@Test
	public void behavior_test() {
		final StreamBetFeeder streamBetFeeder = new StreamBetFeeder(stream, roulette, betParser);
		when(betParser.apply("string1")).thenReturn(BET);
		when(betParser.apply("string2")).thenReturn(empty());
		streamBetFeeder.run();
		verify(betParser).apply("string1");
		verify(betParser).apply("string2");
		verify(roulette).newBet(BET.get());
		verify(roulette).stop();
		verifyNoMoreInteractions(roulette, betParser);
	}
}
