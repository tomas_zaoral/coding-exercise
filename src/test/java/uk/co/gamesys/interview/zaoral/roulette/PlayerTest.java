package uk.co.gamesys.interview.zaoral.roulette;

import static org.junit.Assert.assertEquals;
import static uk.co.gamesys.interview.zaoral.roulette.Outcome.WIN;

import org.junit.Test;

import com.google.common.testing.EqualsTester;
import uk.co.gamesys.interview.zaoral.roulette.Player;
import uk.co.gamesys.interview.zaoral.roulette.ProcessedBet;

public class PlayerTest {
	private static final Player PLAYER_1 = new Player("user1", 10.5, 20.3);
	private static final Player PLAYER_2 = new Player("user1", 10.5, 20);
	private static final Player PLAYER_3 = new Player("user1", 8, 20.3);
	private static final Player PLAYER_4 = new Player("user2", 10.5, 20.3);
	private static final ProcessedBet PROCESSED_BET = ProcessedBet.newBuilder()
			.setBetAmount(20.7)
			.setBetTarget(10)
			.setOutcome(WIN)
			.setUserName("user2")
			.setWinnings(10.5)
			.build();
	
	@Test
	public void test_equals_contract() {
		new EqualsTester()
			.addEqualityGroup(PLAYER_1, PLAYER_1, PLAYER_1)
			.addEqualityGroup(PLAYER_2, PLAYER_2)
			.addEqualityGroup(PLAYER_3, PLAYER_3)
			.addEqualityGroup(PLAYER_4, PLAYER_4)
			.addEqualityGroup('a', 'a')
			.testEquals();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void null_user_name_throws_iae() {
		new Player(null, 10, 12);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void empty_user_name_throws_iae() {
		new Player("", 10, 12);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void negative_total_win_throws_iae() {
		new Player("username", -10, 12);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void negative_total_bet_throws_iae() {
		new Player("username", 10, -12);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void update_with_other_user_data_throws_iae() {
		PLAYER_1.updateWith(PROCESSED_BET);
	}
	
	@Test
	public void update_with_users_processed_bet_updates_correctly() {
		assertEquals(new Player("user2", 21, 41), PLAYER_4.updateWith(PROCESSED_BET));
	}
	
}
