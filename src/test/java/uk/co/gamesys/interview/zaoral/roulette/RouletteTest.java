package uk.co.gamesys.interview.zaoral.roulette;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import uk.co.gamesys.interview.zaoral.roulette.Bet;
import uk.co.gamesys.interview.zaoral.roulette.GameResults;
import uk.co.gamesys.interview.zaoral.roulette.ProcessedBet;
import uk.co.gamesys.interview.zaoral.roulette.Roulette;

import java.util.Random;
import java.util.function.Consumer;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.*;
import static uk.co.gamesys.interview.zaoral.roulette.Outcome.LOSS;
import static uk.co.gamesys.interview.zaoral.roulette.Outcome.WIN;
import static uk.co.gamesys.interview.zaoral.roulette.SpecialBet.EVEN;
import static uk.co.gamesys.interview.zaoral.roulette.SpecialBet.ODD;

@RunWith(MockitoJUnitRunner.class)
public class RouletteTest {
    private static final Bet BET_1 = new Bet("user1", 1, 10);
    private static final Bet BET_2 = new Bet("user2", 2, 7);
    private static final Bet BET_EVEN = new Bet("userEVEN", EVEN.getIntegerValue(), 100);
    private static final Bet BET_ODD = new Bet("userODD", ODD.getIntegerValue(), 1000);
    private static final ProcessedBet BET_1_WIN = ProcessedBet.newBuilder(BET_1)
            .setOutcome(WIN)
            .setWinnings(36 * BET_1.getBetAmount())
            .build();
    private static final ProcessedBet BET_2_WIN = ProcessedBet.newBuilder(BET_2)
            .setOutcome(WIN)
            .setWinnings(36 * BET_2.getBetAmount())
            .build();
    private static final ProcessedBet BET_EVEN_WIN = ProcessedBet.newBuilder(BET_EVEN)
            .setOutcome(WIN)
            .setWinnings(2 * BET_EVEN.getBetAmount())
            .build();
    private static final ProcessedBet BET_ODD_WIN = ProcessedBet.newBuilder(BET_ODD)
            .setOutcome(WIN)
            .setWinnings(2 * BET_ODD.getBetAmount())
            .build();
    private static final ProcessedBet BET_1_LOSS = ProcessedBet.newBuilder(BET_1)
            .setOutcome(LOSS)
            .setWinnings(0)
            .build();
    private static final ProcessedBet BET_2_LOSS = ProcessedBet.newBuilder(BET_2)
            .setOutcome(LOSS)
            .setWinnings(0)
            .build();
    private static final ProcessedBet BET_EVEN_LOSS = ProcessedBet.newBuilder(BET_EVEN)
            .setOutcome(LOSS)
            .setWinnings(0)
            .build();
    private static final ProcessedBet BET_ODD_LOSS = ProcessedBet.newBuilder(BET_ODD)
            .setOutcome(LOSS)
            .setWinnings(0)
            .build();

    @Mock Random random;
    @Mock Consumer<GameResults> gameResultsConsumer;
    Roulette roulette;

    @Before
    public void before() {
        roulette = new Roulette(random, 1, gameResultsConsumer);
    }

    @Test
    public void number_1_drawn() throws InterruptedException {
        when(random.nextInt(Roulette.ROULETTE_HIGHEST_NUMBER + 1)).thenReturn(BET_1.getBetTarget());

        final Thread rouletteEngine = new Thread(roulette);
        rouletteEngine.start();
        roulette.newBet(BET_1);
        roulette.newBet(BET_2);
        roulette.newBet(BET_EVEN);
        roulette.newBet(BET_ODD);
        roulette.stop();
        rouletteEngine.join();

        verify(gameResultsConsumer).accept(new GameResults(BET_1.getBetTarget(), asList(
                BET_1_WIN, BET_2_LOSS, BET_EVEN_LOSS, BET_ODD_WIN)));
        verify(random).nextInt(Roulette.ROULETTE_HIGHEST_NUMBER + 1);
        verifyNoMoreInteractions(random, gameResultsConsumer);
    }

    @Test
    public void number_2_drawn() throws InterruptedException {
        when(random.nextInt(Roulette.ROULETTE_HIGHEST_NUMBER + 1)).thenReturn(BET_2.getBetTarget());

        final Thread rouletteEngine = new Thread(roulette);
        rouletteEngine.start();
        roulette.newBet(BET_1);
        roulette.newBet(BET_2);
        roulette.newBet(BET_EVEN);
        roulette.newBet(BET_ODD);
        roulette.stop();
        rouletteEngine.join();

        verify(gameResultsConsumer).accept(new GameResults(BET_2.getBetTarget(), asList(
                BET_1_LOSS, BET_2_WIN, BET_EVEN_WIN, BET_ODD_LOSS)));
        verify(random).nextInt(Roulette.ROULETTE_HIGHEST_NUMBER + 1);
        verifyNoMoreInteractions(random, gameResultsConsumer);
    }

    @Test
    public void number_0_drawn() throws InterruptedException {
        when(random.nextInt(Roulette.ROULETTE_HIGHEST_NUMBER + 1)).thenReturn(0);

        final Thread rouletteEngine = new Thread(roulette);
        rouletteEngine.start();
        roulette.newBet(BET_1);
        roulette.newBet(BET_2);
        roulette.newBet(BET_EVEN);
        roulette.newBet(BET_ODD);
        roulette.stop();
        rouletteEngine.join();

        verify(gameResultsConsumer).accept(new GameResults(0, asList(
                BET_1_LOSS, BET_2_LOSS, BET_EVEN_LOSS, BET_ODD_LOSS)));
        verify(random).nextInt(Roulette.ROULETTE_HIGHEST_NUMBER + 1);
        verifyNoMoreInteractions(random, gameResultsConsumer);
    }
}