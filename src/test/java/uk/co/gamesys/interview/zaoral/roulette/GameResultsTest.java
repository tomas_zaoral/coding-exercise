package uk.co.gamesys.interview.zaoral.roulette;

import static java.util.Arrays.asList;
import static uk.co.gamesys.interview.zaoral.roulette.Outcome.LOSS;

import org.junit.Test;

import com.google.common.testing.EqualsTester;
import uk.co.gamesys.interview.zaoral.roulette.GameResults;
import uk.co.gamesys.interview.zaoral.roulette.Outcome;
import uk.co.gamesys.interview.zaoral.roulette.ProcessedBet;

public class GameResultsTest {
	private static final ProcessedBet PROCESSED_BET_1 = ProcessedBet
		.newBuilder()
		.setBetAmount(10)
		.setBetTarget(11)
		.setOutcome(LOSS)
		.setUserName("username")
		.setWinnings(0)
		.build();
	private static final ProcessedBet PROCESSED_BET_2 = ProcessedBet
			.newBuilder()
			.setBetAmount(11)
			.setBetTarget(12)
			.setOutcome(Outcome.WIN)
			.setUserName("username2")
			.setWinnings(396)
			.build();
	private final GameResults gameResults1 = new GameResults(1, asList(PROCESSED_BET_1, PROCESSED_BET_2));
	private final GameResults gameResults2 = new GameResults(1, asList());
	private final GameResults gameResults3 = new GameResults(2, asList());
	
	@Test
	public void test_equals_contract() {
		new EqualsTester()
			.addEqualityGroup(gameResults1, gameResults1, gameResults1)
			.addEqualityGroup(gameResults2, gameResults2)
			.addEqualityGroup(gameResults3, gameResults3)
			.addEqualityGroup(1, 1)
			.testEquals();
	}
}
