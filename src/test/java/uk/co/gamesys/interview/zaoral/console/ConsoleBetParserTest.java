package uk.co.gamesys.interview.zaoral.console;

import static java.lang.String.format;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static uk.co.gamesys.interview.zaoral.roulette.SpecialBet.EVEN;
import static uk.co.gamesys.interview.zaoral.roulette.SpecialBet.ODD;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import uk.co.gamesys.interview.zaoral.console.ConsoleBetParser;
import uk.co.gamesys.interview.zaoral.roulette.Bet;
import uk.co.gamesys.interview.zaoral.roulette.Players;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleBetParserTest {
	private static final String USER_NAME = "userName";
	
	@Mock
    Players players;
	ConsoleBetParser parser;

	@Before
	public void before() {
		parser = new ConsoleBetParser(players);
	}
	
	@Test(expected = NullPointerException.class)
	public void test_null_players_in_constructor() {
		new ConsoleBetParser(null);
	}
	
	@Test
	public void test_unknown_user_name() {
		when(players.contain(USER_NAME)).thenReturn(false);
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s EVEN 20", USER_NAME)));
		verify(players).contain(USER_NAME);
		verifyNoMoreInteractions(players);
	}
	
	@Test
	public void test_invalid_bet_target() {
		when(players.contain(USER_NAME)).thenReturn(true);
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s elven 20", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s 0 20", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s 37 20", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s OTT 20", USER_NAME)));
		verify(players, times(4)).contain(USER_NAME);
		verifyNoMoreInteractions(players);
	}
	
	@Test
	public void test_invalid_bet_amount() {
		when(players.contain(USER_NAME)).thenReturn(true);
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s ODD -20", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s EVEN 0", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s 1", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s 7 -0.3", USER_NAME)));
		verify(players, times(3)).contain(USER_NAME);
		verifyNoMoreInteractions(players);
	}
	
	@Test
	public void test_invalid_bet_format() {
		assertEquals(Optional.<Bet>empty(), parser.parse(""));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s,EVEN,0", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s abc def", USER_NAME)));
		assertEquals(Optional.<Bet>empty(), parser.parse(format("%s foobar", USER_NAME)));
		verify(players, times(1)).contain(USER_NAME);
		verifyNoMoreInteractions(players);
	}
	
	@Test
	public void test_correct_values_properly_parsed() {
		when(players.contain(USER_NAME)).thenReturn(true);
		assertEquals(Optional.of(new Bet(USER_NAME, ODD.getIntegerValue(), 10.5)), parser.parse(format("%s ODD 10.5", USER_NAME)));
		assertEquals(Optional.of(new Bet(USER_NAME, EVEN.getIntegerValue(), 12)), parser.parse(format("%s EVEN 12.0", USER_NAME)));
		assertEquals(Optional.of(new Bet(USER_NAME, 1, 0.3)), parser.parse(format("%s 1 0.3", USER_NAME)));
		assertEquals(Optional.of(new Bet(USER_NAME, 36, 7)), parser.parse(format("%s 36 7", USER_NAME)));
		verify(players, times(4)).contain(USER_NAME);
		verifyNoMoreInteractions(players);
	}
}
