package uk.co.gamesys.interview.zaoral.console;

import uk.co.gamesys.interview.zaoral.roulette.Bet;
import uk.co.gamesys.interview.zaoral.roulette.Players;

import java.util.Optional;

import static com.google.common.base.MoreObjects.toStringHelper;
import static com.google.common.base.Preconditions.checkNotNull;
import static uk.co.gamesys.interview.zaoral.roulette.SpecialBet.EVEN;
import static uk.co.gamesys.interview.zaoral.roulette.SpecialBet.ODD;

/**
 * {@code ConsoleBetParser} provides functionality for parsing command line bets.
 * For a bet to be correct following conditions but be met:
 * <ul>
 *  <li>user who submitted the bet must be present in {@code Players} object passed to
 *  {@link ConsoleBetParser#ConsoleBetParser(uk.co.gamesys.interview.zaoral.roulette.Players)}</li>
 *  <li>bet amount must be a positive, non-zero number</li>
 *  <li>bet target must be a valid roulette bet (number 1-36 or any of {@link uk.co.gamesys.interview.zaoral.roulette.SpecialBet} values)</li>
 * </ul>
 */
public final class ConsoleBetParser {
	private final Players players;

	/**
	 * Creates new console bet parser.
	 * 
	 * @param players players that can place a bet, unknown players can't place a bet.
	 */
	public ConsoleBetParser(Players players) {
		this.players = checkNotNull(players);
	}

	/**
	 * Parses bet from given line of text.
	 * 
	 * @param line one line that should represent a bet
	 * @return {@code Optional.of(Bet)} when the format of bet is correct and was
	 * submitted by existing user or {@code Optional.empty()} if line could not
	 * be parsed correctly or the user who submitted it does not exist.  
	 */
	public Optional<Bet> parse(String line) {
		final String[] values = line.split(" ");
		if (values.length != 3) {
			System.err.println("Incorrect bet format: " + line);
			return Optional.empty();
		} else {
			final Optional<String> userName = checkUserName(values[0]);
			final Optional<Integer> betTarget = parseBetTarget(values[1]);
			final Optional<Double> betAmount = parseAmount(values[2]);
			
			return userName.flatMap(uName ->
				betTarget.flatMap(bTarget ->
					betAmount.map(bAmount ->
						new Bet(uName, bTarget, bAmount))));
		}
	}
	

	private Optional<String> checkUserName(String userName) {
		if (players.contain(userName)) {
			return Optional.of(userName);
		} else {
			System.err.println("Unknown user: " + userName);
			return Optional.empty();
		}
	}
	
	private Optional<Integer> parseBetTarget(String betTarget) {
		switch (betTarget) {
		case "EVEN":
			return Optional.of(EVEN.getIntegerValue());
		case "ODD":
			return Optional.of(ODD.getIntegerValue());
		default:
			return parseRouletteInteger(betTarget);
		}
	}

	private Optional<Integer> parseRouletteInteger(String betTarget) {
        try {
            final int number = Integer.parseInt(betTarget);
            if (number > 0 && number < 37) {
                return Optional.of(number);
            }
        } catch (NumberFormatException e) {
            System.err.println("Incorrect bet target: " + betTarget);
        }
        return Optional.empty();
    }
	
	private Optional<Double> parseAmount(String betAmount) {
		try {
			return parsePositiveAmount(betAmount);
		} catch (NumberFormatException e) {
			System.err.println("Incorrect bet amount: " + betAmount);
			e.printStackTrace();
			return Optional.empty();
		}
	}

	private Optional<Double> parsePositiveAmount(String betAmount) {
		final double amount = Double.parseDouble(betAmount);
		if (amount > 0) {
			return Optional.of(amount);
		} else {
			System.err.println("Bet amount must be a positive number.");
			return Optional.empty();
		}
	}
	
	@Override
	public String toString() {
		return toStringHelper("ConsoleBetParser")
				.add("players", players)
				.toString();
	}
}
