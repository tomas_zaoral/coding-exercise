package uk.co.gamesys.interview.zaoral.roulette;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.function.Function;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Contains logic for reading input stream. Parses bet commands, using
 * given bet parser, present in the stream and feeds them to the given
 * roulette engine. Example input:
 * <pre>
 * Tiki_Monkey 2 1.0
 * Barbara EVEN 3.0
 * Tom ODD 6
 * </pre>
 */
public class StreamBetFeeder implements Runnable {
	private final InputStream in;
	private final Roulette roulette;
	private final Function<String, Optional<Bet>> betParser;

	/**
	 * Creates new instance of {@code StreamBetFeeder} for given input stream,
	 * using given bet parser and feeding to provided roulette engine.
	 * 
	 * @param in stream containing commands separated by new line
	 * @param roulette roulette engine that will receive parsed commands
	 * @param betParser parser that can parse a bet from a line of text
	 */
	public StreamBetFeeder(InputStream in, Roulette roulette, Function<String, Optional<Bet>> betParser) {
		this.in = checkNotNull(in);
		this.roulette = checkNotNull(roulette);
		this.betParser = checkNotNull(betParser);
	}

	@Override
	public void run() {
		try {
            try (final InputStreamReader streamReader = new InputStreamReader(in);
                 final BufferedReader bufferedIn = new BufferedReader(streamReader)) {
                String line;
                while ((line = bufferedIn.readLine()) != null) {
                    betParser.apply(line).ifPresent(roulette::newBet);
                }
            } catch (IOException e) {
                System.err.println("An exception occurred while reading from input.");
                e.printStackTrace();
            }
        } finally {
            roulette.stop();
        }
	}
}
