package uk.co.gamesys.interview.zaoral.roulette;

import java.util.List;

import static com.google.common.base.MoreObjects.toStringHelper;
import static com.google.common.base.Objects.equal;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.hash;

/**
 * Value object for holding results of a single game (one ball draw and processed 
 * bets that were placed on this draw).
 */
public final class GameResults {
	private final int number;
	private final List<ProcessedBet> processedBets;
	
	/**
	 * Creates new {@link GameResults} object with given number on which the ball landed and
	 * an list of processed bets.
	 * 
	 * @param number drawn number
	 * @param processedBets all processed bets placed on this draw
	 */
	public GameResults(int number, List<ProcessedBet> processedBets) {
		this.number = number;
		this.processedBets = unmodifiableList(processedBets);
	}

	public int getNumber() {
		return number;
	}

	public List<ProcessedBet> getProcessedBets() {
		return processedBets;
	}

	@Override
	public String toString() {
		return toStringHelper("GameResults")
				.add("number", number)
				.add("processedBets", processedBets)
				.toString();
	}

	@Override
	public int hashCode() {
		return hash(number, processedBets);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (!(obj instanceof GameResults)) {
			return false;
		} else {
			final GameResults other = (GameResults) obj;
			return equal(number, other.number)
					&& equal(processedBets, other.processedBets);
		}
	}
}
