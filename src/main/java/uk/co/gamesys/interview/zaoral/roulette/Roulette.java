package uk.co.gamesys.interview.zaoral.roulette;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.Executors.newSingleThreadExecutor;
import static java.util.stream.Collectors.toList;

/**
 * Roulette engine. Accepts bets from via {@link Roulette#newBet(Bet)} method.
 * Once in a given spin period generates new random number via given RNG,
 * processes all placed bets and feeds games results to a given game results
 * consumer.
 */
public class Roulette implements Runnable {
    public static final int ROULETTE_HIGHEST_NUMBER = 36;
    private final ExecutorService executorService;
	private final List<Bet> bets;
	private final Random random;
	private final Consumer<GameResults> gameResultsConsumer;
	private final long spinPeriodMillis;
    private final Lock betsLock;
	private volatile boolean end = false;

	/**
	 * Creates new instance of roulette engine. Uses given RNG to produce
	 * new random integer in range 1-36 every {@code spinPeriod} milliseconds.
	 * Then processes all bets that were placed from previous run and feeds
	 * game results to a given game results consumer.
	 * 
	 * @param random RNG to be used for generating random numbers (drawn number)
	 * @param spinPeriodMillis specifies how often a number will be drawn
	 * @param gameResultsConsumer consumer of game results, is fed with game results
	 * after each draw
	 */
	public Roulette(Random random, long spinPeriodMillis, Consumer<GameResults> gameResultsConsumer) {
        checkArgument(spinPeriodMillis > 0);
		this.bets = new LinkedList<>();
		this.random = checkNotNull(random);
		this.executorService = newSingleThreadExecutor();
		this.gameResultsConsumer = checkNotNull(gameResultsConsumer);
		this.spinPeriodMillis = spinPeriodMillis;
        this.betsLock = new ReentrantLock(true); // using fair lock to avoid roulette engine starvation
	}

    @Override
    public void run() {
        try {
            while (!end || hasMoreBets()) {
                sleepFor(spinPeriodMillis);
                final List<Bet> placedBets = takeAllCurrentBets();
                executorService.execute(() -> {
                    final int number = random.nextInt(ROULETTE_HIGHEST_NUMBER + 1);
                    final List<ProcessedBet> processedBets = processBets(number, placedBets);
                    gameResultsConsumer.accept(new GameResults(number, processedBets));
                });
            }
        } finally {
            shutDownAndAwaitOneMinute();
        }
    }

    /**
	 * Places new bet. All bets are processed once in a {@code spinPeriod} given
	 * as parameter to {@link #Roulette(Random, long, Consumer)}. If {@link Roulette#stop()}
	 * was already invoked, bet will not be placed!
	 *
	 * @param bet to be placed bet
	 */
	public void newBet(Bet bet) {
		if (!end) {
            betsLock.lock();
			try {
                bets.add(bet);
            } finally {
                betsLock.unlock();
            }
        }
	}

	/**
	 * Call this method when engine should shutdown.
	 * Once called, engine will shutdown softly - meaning
	 * that no more bets will be accepted but bets that are
	 * already placed will be processed before shutdown (on next closest spin).
	 */
	public void stop() {
		end = true;
	}

    private boolean hasMoreBets() {
        betsLock.lock();
        try {
            return bets.size() > 0;
        } finally {
            betsLock.unlock();
        }
    }

    private List<Bet> takeAllCurrentBets() {
        betsLock.lock();
        try {
            final List<Bet> placedBets = new ArrayList<>(bets);
            bets.clear();
            return placedBets;
        } finally {
            betsLock.unlock();
        }
    }

    private void shutDownAndAwaitOneMinute() {
        try {
            executorService.shutdown();
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            System.err.println("Executor service did not shut down.");
        }
    }


    private List<ProcessedBet> processBets(int number, List<Bet> placedBets) {
        return placedBets
                .stream()
                .map(placedBet -> {
                    final boolean isWin = isWin(number, placedBet.getBetTarget());
                    final double winnings = isWin ? computeWinnings(placedBet) : 0;
                    return ProcessedBet.newBuilder(placedBet)
                            .setOutcome(isWin ? Outcome.WIN : Outcome.LOSS)
                            .setWinnings(winnings)
                            .build();
                })
                .collect(toList());
    }

    private double computeWinnings(Bet placedBet) {
		if (placedBet.getBetTarget() == SpecialBet.EVEN.getIntegerValue()
				|| placedBet.getBetTarget() == SpecialBet.ODD.getIntegerValue()) {
			return placedBet.getBetAmount() * 2;
		} else {
			return placedBet.getBetAmount() * 36;
		}
	}

	private boolean isWin(int number, int betTarget) {
		if (number == 0) {
			return false;
		} else if (betTarget == SpecialBet.EVEN.getIntegerValue()) {
			return (number & 1) == 0;
		} else if (betTarget == SpecialBet.ODD.getIntegerValue()) {
			return (number & 1) == 1;
		} else {
			return number == betTarget;
		}
	}

	private void sleepFor(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ignored) {
			// intentionally left empty
		}
	}
}
