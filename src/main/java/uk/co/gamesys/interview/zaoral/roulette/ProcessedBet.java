package uk.co.gamesys.interview.zaoral.roulette;

import com.google.common.base.MoreObjects;

import java.util.Optional;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.hash;

/**
 * Represents a bet that has been processed (typically by a roulette engine).
 * Processed bet is a bet that is created from {@link Bet Bet} object and
 * in addition contains outcome of the bet and winnings.
 *
 * <p>
 * <i>Use {@link #newBuilder()} to create new instances.</i>
 * </p>
 */
public final class ProcessedBet {
	
	/**
	 * Return new builder for {@code ProcessedBet} object.
	 * 
	 * @return new {@code ProcessedBet} builder.
	 */
	public static Builder newBuilder() {
		return new Builder();
	}

    /**
     * Return new builder for {@code ProcessedBet} object.
     * With preset values from given {@code bet} object.
     *
     * @param bet used to preset values on returned builder
     * @return new {@code ProcessedBet} builder with fields: {@code betAmount, betTarget, userName}
     * preset from give bet object.
     */
    public static Builder newBuilder(Bet bet) {
        checkNotNull(bet);
        return new Builder()
                .setBetAmount(bet.getBetAmount())
                .setBetTarget(bet.getBetTarget())
                .setUserName(bet.getUserName());
    }
	
	private final String userName;
	private final double betAmount;
	private final int betTarget;
	private final Outcome outcome;
	private final double winnings;
	
	private ProcessedBet(String userName, double betAmount, int betTarget,
			Outcome outcome, double winnings) {
		this.userName = userName;
		this.betAmount = betAmount;
		this.betTarget = betTarget;
		this.outcome = outcome;
		this.winnings = winnings;
	}
	
	public String getUserName() {
		return userName;
	}

	public double getBetAmount() {
		return betAmount;
	}

	public int getBetTarget() {
		return betTarget;
	}

	public Outcome getOutcome() {
		return outcome;
	}

	public double getWinnings() {
		return winnings;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper("ProcessedBet.Builder")
				.add("userName", userName)
				.add("betAmount", betAmount)
				.add("betTarget", betTarget)
				.add("outcome", outcome)
				.add("winnings", winnings)
				.toString();
	}

	@Override
	public int hashCode() {
		return hash(userName, betAmount, betTarget, outcome, winnings);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (!(obj instanceof ProcessedBet)) {
			return false;
		} else {
			final ProcessedBet other = (ProcessedBet) obj;
			return equal(userName, other.userName)
					&& equal(betAmount, other.betAmount)
					&& equal(betTarget, other.betTarget)
					&& equal(outcome, other.outcome)
					&& equal(winnings, other.winnings);
		}
	}

	public static final class Builder {
		private Optional<String> userName = Optional.empty();
		private Optional<Double> betAmount = Optional.empty();
		private Optional<Integer> betTarget = Optional.empty();
		private Optional<Outcome> outcome = Optional.empty();
		private Optional<Double> winnings = Optional.empty();
		
		private Builder() {
		}
		
		public ProcessedBet build() {
			if (userName.isPresent()
					&& betAmount.isPresent()
					&& betTarget.isPresent()
					&& outcome.isPresent()
					&& winnings.isPresent()) {
				return new ProcessedBet(userName.get(), betAmount.get(), betTarget.get(), outcome.get(), winnings.get());
			} else {
				throw new IllegalStateException("Not all values were set while building ProcessedBet. " + this.toString());
			}
		}
		
		public Optional<String> getUserName() {
			return userName;
		}
		public Builder setUserName(String userName) {
			this.userName = Optional.of(userName);
			return this;
		}
		public Optional<Double> getBetAmount() {
			return betAmount;
		}
		public Builder setBetAmount(double betAmount) {
			this.betAmount = Optional.of(betAmount);
			return this;
		}
		public Optional<Integer> getBetTarget() {
			return betTarget;
		}
		public Builder setBetTarget(int betTarget) {
			this.betTarget = Optional.of(betTarget);
			return this;
		}
		public Optional<Outcome> getOutcome() {
			return outcome;
		}
		public Builder setOutcome(Outcome outcome) {
			this.outcome = Optional.of(outcome);
			return this;
		}
		public Optional<Double> getWinnings() {
			return winnings;
		}
		public Builder setWinnings(double winnings) {
			this.winnings = Optional.of(winnings);
			return this;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("ProcessedBet.Builder")
					.add("userName", userName)
					.add("betAmount", betAmount)
					.add("betTarget", betTarget)
					.add("outcome", outcome)
					.add("winnings", winnings)
					.toString();
		}

		@Override
		public int hashCode() {
			return hash(userName, betAmount, betTarget, outcome, winnings);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			} else if (!(obj instanceof ProcessedBet)) {
				return false;
			} else {
				final ProcessedBet other = (ProcessedBet) obj;
				return equal(userName, other.userName)
						&& equal(betAmount, other.betAmount)
						&& equal(betTarget, other.betTarget)
						&& equal(outcome, other.outcome)
						&& equal(winnings, other.winnings);
			}
		}
	}
}