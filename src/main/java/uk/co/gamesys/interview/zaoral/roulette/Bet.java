package uk.co.gamesys.interview.zaoral.roulette;

import static com.google.common.base.MoreObjects.toStringHelper;
import static com.google.common.base.Objects.equal;
import static java.util.Objects.hash;

/**
 * Represents a bet that was placed by a user but not executed by the roulette.
 * i.e.: bets placed on the roulette board
 *
 */
public final class Bet {
	private final String userName;
	private final int betTarget;
	private final double betAmount;
	
	/**
	 * Creates new bet with given values;
	 * 
	 * @param userName name of the user who made this bet
	 * @param betTarget specifies what number/special bet a user made
	 * @param betAmount amount of money bet
	 */
	public Bet(String userName, int betTarget, double betAmount) {
		this.userName = userName;
		this.betTarget = betTarget;
		this.betAmount = betAmount;
	}

	public String getUserName() {
		return userName;
	}

	public int getBetTarget() {
		return betTarget;
	}

	public double getBetAmount() {
		return betAmount;
	}

	@Override
	public String toString() {
		return toStringHelper("Bet")
				.add("userName", userName)
				.add("betTarget", betTarget)
				.add("betAmount", betAmount)
				.toString();
	}

	@Override
	public int hashCode() {
		return hash(userName, betTarget, betAmount);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (!(obj instanceof Bet)) {
			return false;
		} else {
			final Bet other = (Bet) obj;
			return equal(userName, other.userName)
					&& equal(betTarget, other.betTarget)
					&& equal(betAmount, other.betAmount);
		}
	}
}
