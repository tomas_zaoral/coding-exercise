package uk.co.gamesys.interview.zaoral.roulette;

/**
 * Enumerates possible outcomes for a bet (loss or win). 
 */
public enum Outcome {
	LOSS, WIN
}
