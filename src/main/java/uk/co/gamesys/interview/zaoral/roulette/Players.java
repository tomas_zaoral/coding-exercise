package uk.co.gamesys.interview.zaoral.roulette;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.reader.CSVReader;
import com.googlecode.jcsv.reader.internal.CSVReaderBuilder;
import uk.co.gamesys.interview.zaoral.console.ConsoleFormatter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.google.common.base.MoreObjects.toStringHelper;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableCollection;
import static java.util.stream.Collectors.toMap;

/**
 * Class representing players database.
 * 
 * Players are loaded during the application start up.
 */
public class Players implements Iterable<Player> {
	private final Map<String, Player> players;
	private final int longestNameLength;

	/**
	 * Creates new {@code Players} object filled with users from
	 * CSV file given as a parameter. Duplicate users are ignored
	 * 
	 * @param fileName name of the file containing CSV definitions of users
	 */
	public Players(String fileName) {
		this.players = loadPlayersFromFile(checkNotNull(fileName));
		this.longestNameLength = findLongestNameLength();
	}

	/**
	 * Test for user's existence.
	 * 
	 * @param userName name of the user being looked up
	 * @return true if such user exists, false otherwise
	 */
	public boolean contain(String userName) {
		return players.keySet().contains(userName);
	}
	
	/**
	 * Iterates all processed bets present in a {@code GameResults} object
	 * and updates according users with new data.
	 *  
	 * @param results game results that will be used to update according user's statistics
	 */
	public synchronized void updateWithResults(GameResults results) {
		results.getProcessedBets().forEach(processedBet -> {
			final Player player = players.get(processedBet.getUserName());
			players.put(player.getUserName(), player.updateWith(processedBet));
		});
	}

    @Override
    public synchronized Iterator<Player> iterator() {
        return unmodifiableCollection(new LinkedList<>(players.values())).iterator();
    }

	public int getLongestNameLength() {
		return longestNameLength;
	}

	private Map<String, Player> loadPlayersFromFile(String fileName) {
		try (final FileReader fileReader = new FileReader(fileName)) {
			return parseUsersFromCsv(fileReader); 
		} catch (FileNotFoundException e) {
			System.err.println(format("File with name %s could not be found.", fileName));
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println(format("IOException occurred while working with file %s.", fileName));
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.err.println(format("A number was expected, but received something different."));
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("Bad user data format.");
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("An exception was raised while processing users file.");
			e.printStackTrace();
		}
		System.err.println("Users were not loaded!");
		return emptyMap();
	}

	private Map<String, Player> parseUsersFromCsv(final FileReader fileReader) throws IOException {
		final CSVReader<Player> csvReader = new CSVReaderBuilder<Player>(fileReader)
				.entryParser(Player.entryParser())
				.strategy(CSVStrategy.UK_DEFAULT)
				.build();
		return csvReader
				.readAll()
				.stream()
				.collect(toMap(Player::getUserName,
						p -> p,
						(p1, p2) -> {
							System.err.println("Duplicate user name: " + p1.getUserName());
							return p1;
						},
                        LinkedHashMap::new));
	}
	
	private Integer findLongestNameLength() {
		return players
				.keySet()
				.stream()
				.reduce(ConsoleFormatter.PLAYER_WORD_LENGTH,
						(x, s) -> {
							final int len = s.length();
							return x < len ? len : x;
						}, (x, y) -> x > y ? x : y);
	}

	@Override
	public String toString() {
		return toStringHelper("Players")
				.add("players", players)
				.add("longestNameLength", longestNameLength)
				.toString();
	}
}
