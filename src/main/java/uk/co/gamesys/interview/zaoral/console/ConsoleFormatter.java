package uk.co.gamesys.interview.zaoral.console;

import com.google.common.base.MoreObjects;
import uk.co.gamesys.interview.zaoral.roulette.*;

import static java.lang.String.format;
import static uk.co.gamesys.interview.zaoral.roulette.Outcome.WIN;
import static uk.co.gamesys.interview.zaoral.roulette.SpecialBet.EVEN;

/**
 * Utility class for creating console-friendly string representation of other objects.
 */
public final class ConsoleFormatter {
	public static final int PLAYER_WORD_LENGTH = 6;
	private final int longestName;
	
	/**
	 * Creates new console formatter.
	 * 
	 * @param longestPlayerNameLength length of longest player name - needing for formatting of
	 * player name column
	 */
	public ConsoleFormatter(int longestPlayerNameLength) {
		this.longestName = longestPlayerNameLength;
	}
	
	/**
	 * Creates console string from game results.
	 * Example:
	 * <br><br>
	 * <pre>
	 * Number: 30
	 * Player  Bet   Outcome  Winnings
	 * ---
	 * Rick   EVEN       WIN       8.0
	 * Tom     ODD     LOOSE       0.0
	 * </pre>
	 * @param gameResults results of a game for which console string will be created.
	 * @return console string representing given game results
	 */
	public String createConsoleString(GameResults gameResults) {
		final StringBuilder sb = new StringBuilder();
		
		sb.append(format("Number: %2d\n", gameResults.getNumber()));
		sb.append(format("Player%" + (longestName - PLAYER_WORD_LENGTH + 2) + "s", ""));
		sb.append(format("Bet   Outcome  Winnings\n"));
		sb.append("---\n");
		for (ProcessedBet processedBet : gameResults.getProcessedBets()) {
            appendProcessedBetRow(sb, processedBet);
		}
		return sb.toString();
	}

    private void appendProcessedBetRow(StringBuilder sb, ProcessedBet processedBet) {
        final int uNameLen = processedBet.getUserName().length();
        final int target = processedBet.getBetTarget();
        sb.append(format("%s%" + (longestName - uNameLen + 2) + "s", processedBet.getUserName(), ""));
        sb.append(target == EVEN.getIntegerValue() ? "EVEN  "
                : (target == SpecialBet.ODD.getIntegerValue() ? " ODD  "
                    : format("  %2d  ", target)));
        sb.append(processedBet.getOutcome() == WIN ? "    WIN  " : "  LOOSE  ");
        sb.append(format("%8.1f\n", processedBet.getWinnings()));
    }

    /**
	 * Creates console string from list of players. Example:
	 * <br><br>
	 * <pre>
	 * Player  Total Win  Total Bet
	 * ---
	 * Rick         18.5       25.0
	 * Tom           0.0       11.0
	 * </pre>
	 * 
	 * @param players players object that will be converted to a console string.
	 * @return console string representing given {@code Players} object.
	 */
	public String createConsoleString(Players players) {
		final StringBuilder sb = new StringBuilder();

		sb.append(format("Player%" + (longestName - PLAYER_WORD_LENGTH + 2) + "s", ""));
		sb.append("Total Win  Total Bet\n");
		sb.append("---\n");
		for (Player player : players) {
            appendUsersRow(sb, player);
		}
		
		return sb.toString();
	}

    private void appendUsersRow(StringBuilder sb, Player player) {
        final int uNameLen = player.getUserName().length();
        sb.append(format("%s%" + (longestName - uNameLen + 2) + "s", player.getUserName(), ""));
        sb.append(format("%9.1f  ", player.getTotalWin()));
        sb.append(format("%9.1f\n", player.getTotalBet()));
    }

    @Override
	public String toString() {
		return MoreObjects.toStringHelper("ConsoleFormatter")
				.add("longestName", longestName)
				.toString();
	}
}