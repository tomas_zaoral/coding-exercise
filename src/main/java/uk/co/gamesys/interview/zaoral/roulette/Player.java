package uk.co.gamesys.interview.zaoral.roulette;

import com.google.common.base.MoreObjects;
import com.googlecode.jcsv.reader.CSVEntryParser;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Double.parseDouble;
import static java.util.Objects.hash;

/**
 * Represents a roulette player.
 * Player must have unique user name. On top of that
 * statistics for each player are monitored (total win, total bet).
 */
public final class Player {
	/**
	 * Static factory method for player CSV parser.
     *
	 * @return parser for CSV representing a user
	 */
	public static CSVEntryParser<Player> entryParser() {
		return (data) -> {
			final String userName = data[0];
			final double totalWin = (data.length > 1) ? parseDouble(data[1]) : 0;
			final double totalBet = (data.length > 2) ? parseDouble(data[2]) : 0;
			return new Player(userName, totalWin, totalBet);
		};
	}
	
	private final String userName;
	private final double totalWin;
	private final double totalBet;
	
	/**
	 * Creates new player object with given values.
	 * 
	 * @param userName name of the user
	 * @param totalWin sum of all winnings user achieved
	 * @param totalBet sum of all bets user has placed
	 */
	public Player(String userName, double totalWin, double totalBet) {
		checkArgument(userName != null && !userName.isEmpty());
		checkArgument(totalWin >= 0);
		checkArgument(totalBet >= 0);
		
		this.userName = userName;
		this.totalWin = totalWin;
		this.totalBet = totalBet;
	}
	
	/**
	 * Updates player statistics with values from processed bet.
	 * 
	 * @param processedBet user's bet that has been processed (usually
	 * by roulette engine)
	 * @return new {@code Player} object with updated statistics
	 */
	public Player updateWith(ProcessedBet processedBet) {
		checkArgument(processedBet.getUserName().equals(userName),
				"Trying to populate one player statistics"
				+ "with other player statistics.");
		return new Player(
				userName,
				totalWin + processedBet.getWinnings(),
				totalBet + processedBet.getBetAmount());
	}
	
	public String getUserName() {
		return userName;
	}

	public double getTotalWin() {
		return totalWin;
	}
	
	public double getTotalBet() {
		return totalBet;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper("Player")
				.add("userName", userName)
				.add("totalWin", totalWin)
				.add("totalBet", totalBet)
				.toString();
	}

	@Override
	public int hashCode() {
		return hash(userName, totalBet, totalWin);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Player)) {
			return false;
		} else {
			final Player other = (Player) obj;
			return equal(userName, other.userName)
					&& equal(totalBet, other.totalBet)
					&& equal(totalWin, other.totalWin);
		}
	}
}
