package uk.co.gamesys.interview.zaoral.console;

import uk.co.gamesys.interview.zaoral.roulette.Players;
import uk.co.gamesys.interview.zaoral.roulette.Roulette;
import uk.co.gamesys.interview.zaoral.roulette.StreamBetFeeder;

import java.util.Random;

import static java.lang.System.currentTimeMillis;

/**
 * Console roulette main class.
 */
public class ConsoleRoulette {
	
	private static final long SPIN_PERIOD_MILLIS = 30000;
	
	private ConsoleRoulette() {
	}

	public static void main(String... args) {
		final String fileName = readFileNameFromArgs(args);
		final Players players = new Players(fileName);
		final ConsoleFormatter consoleFormatter = new ConsoleFormatter(players.getLongestNameLength());
		final Roulette roulette = new Roulette(new Random(currentTimeMillis()), SPIN_PERIOD_MILLIS, results -> {
			players.updateWithResults(results);
			System.out.println(consoleFormatter.createConsoleString(results));
			System.out.println(consoleFormatter.createConsoleString(players));
		});
		final ConsoleBetParser betParser = new ConsoleBetParser(players);
		final StreamBetFeeder rouletteBetFeeder = new StreamBetFeeder(System.in, roulette, betParser::parse);
		
		new Thread(roulette, "RouletteEngine").start();
		new Thread(rouletteBetFeeder, "RouletteBetFeeder").start();
	}

	private static String readFileNameFromArgs(String[] args) {
		if (args.length != 1) {
			throw new IllegalArgumentException("Program accepts only one argument - file name of file containing players.\n"
					+ "File format:<player_name>,<total win>,<total bet>\n"
					+ "Players are separated by new line, total win and total bet values are optional.");
		}
		return args[0];
	}
}
