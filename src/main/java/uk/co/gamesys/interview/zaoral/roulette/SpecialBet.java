package uk.co.gamesys.interview.zaoral.roulette;

/**
 * Enumerates and maps to integer value all known special bets:<br>
 * {@code EVEN(37), ODD(38)}
 */
public enum SpecialBet {
	EVEN(37), ODD(38);
	
	private final int integerValue;
	
	private SpecialBet(int integerValue) {
		this.integerValue = integerValue;
	}

	public int getIntegerValue() {
		return integerValue;
	}
}
